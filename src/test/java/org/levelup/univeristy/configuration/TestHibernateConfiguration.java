package org.levelup.univeristy.configuration;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.levelup.univeristy.domain.Faculty;
import org.levelup.univeristy.domain.FacultyInfo;
import org.levelup.univeristy.domain.Subject;
import org.levelup.univeristy.domain.University;
import org.postgresql.Driver;

import java.util.HashMap;
import java.util.Map;

public class TestHibernateConfiguration {

    private static SessionFactory factory;

    public static SessionFactory getFactory() {
        return factory;
    }

    static {
        Map<String, String> properties = new HashMap<>();

        properties.put("hibernate.connection.driver_class", Driver.class.getName());
        properties.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/university-it");
        properties.put("hibernate.connection.username", "postgres");
        properties.put("hibernate.connection.password", "root");

        properties.put("hibernate.show_sql", "true");         // Выводит в консоль все запросы, которые выполняет Hibernate
        properties.put("hibernate.format_sql", "true");       // Выводит запросы в более читаемом виде
        properties.put("hibernate.hbm2ddl.auto", "create");   // Отвечает за генерацию схемы

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        Configuration cfg = new Configuration()
                .addAnnotatedClass(University.class)
                .addAnnotatedClass(Faculty.class)
                .addAnnotatedClass(FacultyInfo.class)
                .addAnnotatedClass(Subject.class);

        factory = cfg.buildSessionFactory(ssr);
    }

    // {}

}
