package org.levelup.univeristy.jdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPoolTest {

    @Test
    // test<имя тестируемого метода>_when<входные параметры/состояние класса>_then<ожидаемый результат>
    // testGetConnection_whenQueueIsEmpty_thenReturnNull
    // shouldReturnNullIfQueueIsEmpty
    @DisplayName("getConnection(): should return null if queue is empty")
    public void shouldReturnNullIfQueueIsEmpty() {
        // given        // given        // where
        // when         // where
        // then

        // given
        ConnectionPool pool = new ConnectionPool();
        // when
        Connection result = pool.getConnection();
        // then
        Assertions.assertNull(result);
    }

    @Test
    public void shouldOfferConnectionToPool() throws SQLException {
        // given
        ConnectionPool pool = new ConnectionPool();
        Connection connection = Mockito.mock(Connection.class); // new instance of Proxy(Connection.class)

        // when
        // void returnConnection(Connection c);
        pool.returnConnection(connection);

        // there
        Connection result = pool.getConnection();
        Assertions.assertSame(connection, result);
    }

}