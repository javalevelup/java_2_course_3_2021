package org.levelup.univeristy.hbm;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.levelup.univeristy.configuration.TestHibernateConfiguration;
import org.levelup.univeristy.domain.Faculty;
import org.levelup.univeristy.domain.University;
import org.levelup.univeristy.repository.hbm.HibernateFacultyRepository;
import org.levelup.univeristy.repository.hbm.HibernateUniversityRepository;

public class HibernateFacultyRepositoryIT {

    private static HibernateUniversityRepository universityRepository;
    private static HibernateFacultyRepository facultyRepository;

    @BeforeAll
    public static void initializeRepositories() {
        universityRepository = new HibernateUniversityRepository(TestHibernateConfiguration.getFactory());
        facultyRepository = new HibernateFacultyRepository(TestHibernateConfiguration.getFactory());
    }

    @Test
    public void shouldCreateFaculty() {
        // given
        University university = universityRepository
                .createUniversity("University_1", "U1", 2000);

        String name = "Faculty_1";

        // when
        Faculty result = facultyRepository.createFaculty(name, university.getUniversityId());

        // then
        Assertions.assertNotNull(result.getId());
        Assertions.assertEquals(name, result.getName());
    }

}
