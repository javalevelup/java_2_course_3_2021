package org.levelup.univeristy.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.levelup.univeristy.repository.hbm.HibernateUniversityRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
// RunWith (JUnit 4)
public class HibernateUniversityRepositoryTest {

    @Mock
    private SessionFactory factory;
    @Mock
    private Session session;

    @InjectMocks
    private HibernateUniversityRepository universityRepository;

//    @BeforeEach
//    public void initializeMocks() {
//        MockitoAnnotations.openMocks(this);
//    }

    @Test
    public void shouldReturnEmptyCollectionOfUniversities() {
        // TODO
    }

}
