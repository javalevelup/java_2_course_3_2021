package org.levelup.univeristy.repository;

import org.levelup.univeristy.domain.Faculty;

public interface FacultyRepository {

    Faculty createFaculty(String name, Long universityId);

}
