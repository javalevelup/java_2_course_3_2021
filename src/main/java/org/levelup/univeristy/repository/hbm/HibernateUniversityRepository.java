package org.levelup.univeristy.repository.hbm;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.univeristy.domain.Faculty;
import org.levelup.univeristy.domain.University;
import org.levelup.univeristy.repository.UniversityRepository;

import java.util.List;

// @RequiredArgsConstructor // конструктор с final полями - public HibernateUniversityRepository(SessionFactory factory) { this.factory = factory; }
public class HibernateUniversityRepository extends AbstractHibernateRepository implements UniversityRepository {

    public HibernateUniversityRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public List<University> findAll() {
        try (Session session = factory.openSession()) {
            // HQL - Hibernate Query Language
            // select * from university where short_name = '...'
            // from University where shortName = '...'
            return session.createQuery("from University", University.class)
                    .getResultList(); // список строк, преобразованных в объекты сущности
        }
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear) {
        return runWithTransaction(session -> {
            University u = new University(name, shortName, foundationYear);
            session.persist(u); // insert
            System.out.println("Newly inserted row ID: " + u.getUniversityId());
            return u;
        });
    }

    @Override
    public University findByUniversityId(long universityId) {
        try (Session session = factory.openSession()) {
            return session.createQuery("from University where universityId = :uid", University.class)
                    .setParameter("uid", universityId)
                    .getSingleResult();
        }
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear, List<String> facultyNames) {
        try (Session session = factory.openSession()) {
            Transaction tx = session.beginTransaction();

            University university = new University(name, shortName, foundationYear);

            for (String facultyName : facultyNames) {
                Faculty faculty = new Faculty();
                faculty.setName(facultyName);
                faculty.setUniversity(university);

                university.getFaculties().add(faculty);
            }

            session.persist(university);

            tx.commit();
            return university;
        }

    }

}
