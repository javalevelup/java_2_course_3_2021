package org.levelup.univeristy.repository.hbm;

import org.hibernate.SessionFactory;
import org.levelup.univeristy.domain.Faculty;
import org.levelup.univeristy.domain.University;
import org.levelup.univeristy.repository.FacultyRepository;

public class HibernateFacultyRepository extends AbstractHibernateRepository implements FacultyRepository {

    public HibernateFacultyRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public Faculty createFaculty(String name, Long universityId) {
        return runWithTransaction(session -> {
            Faculty faculty = new Faculty();
            faculty.setName(name);
            // get/load
            faculty.setUniversity(session.load(University.class, universityId)); // -> university_id -> insert
            session.persist(faculty);
            return faculty;
        });
    }

//    @Override
//    public Faculty createFaculty(String name, Long universityId) {
//        try (Session session = factory.openSession()) {
//            Transaction tx = session.beginTransaction();
//
//            Faculty faculty = new Faculty();
//            faculty.setName(name);
//            // get/load
//            faculty.setUniversity(session.load(University.class, universityId)); // -> university_id -> insert
//
//            session.persist(faculty);
//
//            tx.commit();
//            return faculty;
//        }
//    }

}
