package org.levelup.univeristy.repository.hbm;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.function.Function;

@RequiredArgsConstructor // public AbstractHibernateRepository(SessionFactory factory) { this.factory = factory; }
public abstract class AbstractHibernateRepository {

    protected final SessionFactory factory;

    // "Различия" всегда возвращают какой-либо результат
    protected <R> R runWithTransaction(Function<Session, R> invokeWithTransaction) {
        try (Session session = factory.openSession()) {
            // ACID
            // A - atomicity - атомарность
            // C - consistency - консистеность
            // I - isolation - изоляция транзакций
            // D - durability - доступность (WAL - write ahead log), LSM
            Transaction tx = session.beginTransaction();

            // difference
            R result = invokeWithTransaction.apply(session);

            tx.commit(); // фиксирование транзакции
            return result;
        }
    }

}
