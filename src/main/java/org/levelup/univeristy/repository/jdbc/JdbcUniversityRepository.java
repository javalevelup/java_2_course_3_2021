package org.levelup.univeristy.repository.jdbc;

import org.levelup.univeristy.domain.University;
import org.levelup.univeristy.jdbc.DatabaseService;
import org.levelup.univeristy.repository.UniversityRepository;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JdbcUniversityRepository implements UniversityRepository {

    private final DatabaseService dbService;

    public JdbcUniversityRepository(DatabaseService dbService) {
        this.dbService = dbService;
    }

    @Override
    public List<University> findAll() {
        try (Connection connection = dbService.openConnection()) {
            //
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(JdbcSqls.SELECT_ALL_UNIVERSITIES);

            return retrieveFromResultSet(resultSet);
        } catch (SQLException exc) {
            System.out.println("Couldn't get all university because of an error: " + exc.getMessage());
            return Collections.emptyList();
        }
    }

    // Sql injection:
    //  input text: Dmitry); drop database ...;
    // insert into users (name) values (Dmitry); drop database ...;

    // executeUpdate("inesrt into users(name) values (" + name + ")";
    // Dmitry); drop database ...; --
    // executeUpdate("inesrt into users(name) values (" + name + ")";
    // executeUpdate("inesrt into users(name) values ('Dmitry); drop database ...;' --  )"

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear) {
        try (Connection connection = dbService.openConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("insert into university (name, short_name, foundation_year) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            // ('name', '', ...)
            stmt.setString(1, name);
            stmt.setString(2, shortName);

            if (foundationYear == null) {
                stmt.setNull(3, JDBCType.INTEGER.getVendorTypeNumber());
            } else {
                stmt.setInt(3, foundationYear); // -> NPE : foundationYear.intValue() -> null.intValue() -> NPE
            }

            int createdRows = stmt.executeUpdate(); // insert/delete/update
            System.out.println("Count of inserted rows: " + createdRows);

            ResultSet generatedKeysSet = stmt.getGeneratedKeys();
            generatedKeysSet.next();
            Long universityId = generatedKeysSet.getLong(1);

            return new University(universityId, name, shortName, foundationYear);
//            boolean hasResult = stmt.execute();
//            if (hasResult) {
//                ResultSet resultSet = stmt.getResultSet();
//            }
        } catch (SQLException exc) {
            System.out.println("Couldn't insert new university: " + exc.getMessage());
        }
        return null;
    }

    @Override
    public University findByUniversityId(long universityId) {
        return null;
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear, List<String> facultyNames) {
        return null;
    }

    private List<University> retrieveFromResultSet(ResultSet resultSet) throws SQLException {
        List<University> universities = new ArrayList<>();
        // ResultSet - ~Iterator
        while (resultSet.next()) {
            Long universityId = resultSet.getLong("id");
            String name = resultSet.getString("name");
            String shortName = resultSet.getString("short_name");
            Integer foundationYear = resultSet.getInt("foundation_year");

            University university = new University(universityId, name, shortName, foundationYear);
            universities.add(university);
        }

        return universities;
    }

}
