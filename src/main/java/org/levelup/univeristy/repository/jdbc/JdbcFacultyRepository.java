package org.levelup.univeristy.repository.jdbc;

import org.levelup.univeristy.jdbc.DefaultDatabaseService;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcFacultyRepository {

    private final DefaultDatabaseService dbService;

    public JdbcFacultyRepository(DefaultDatabaseService dbService) {
        this.dbService = dbService;
    }

    public void getAllFaculties() {
        try (Connection connection = dbService.openConnection()) { // теперь соединение берется из connectionPool
            // ...
        } catch (SQLException exc) {

        }
        // connection.close();
    }

}
