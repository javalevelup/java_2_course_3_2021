package org.levelup.univeristy.repository;

import org.levelup.univeristy.domain.University;

import java.util.List;

// Controller
// Service
// DAO - Data Access Object
public interface UniversityRepository {

    List<University> findAll();

    University createUniversity(String name, String shortName, Integer foundationYear);

    University createUniversity(String name, String shortName, Integer foundationYear, List<String> facultyNames);

    University findByUniversityId(long universityId);

}
