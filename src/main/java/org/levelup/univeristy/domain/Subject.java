package org.levelup.univeristy.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "subjects")
@NoArgsConstructor
@ToString(exclude = "faculties")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private int hours;

    @ManyToMany
    @JoinTable(
            name = "subjects_on_faculties",
            joinColumns = @JoinColumn(name = "subject_id"), // колонка из связующей таблицы, которая указывает на ID из таблицы subjects
            inverseJoinColumns = @JoinColumn(name = "faculty_id") // колонка из связующей таблицы, которая указывает на ID из таблицы faculties
    )
    private List<Faculty> faculties;

    public Subject(String name, int hours) {
        this.name = name;
        this.hours = hours;
    }

}
