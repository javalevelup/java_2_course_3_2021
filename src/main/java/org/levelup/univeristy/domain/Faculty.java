package org.levelup.univeristy.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "faculties")
@ToString(exclude = {
        "university",
        "subjects"
}) // toString только двух полей (id, name)
public class Faculty {

    // university - faculties - 1-M
    // id, name, universityId

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    // связь с таблицей University
    // bidirectional - unidirectional

    // 1-M -> OneToMany, ManyToOne
    @ManyToOne
    @JoinColumn(name = "university_id")
    private University university;

    @ManyToMany(mappedBy = "faculties", cascade = CascadeType.PERSIST)
    private List<Subject> subjects;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "faculty_id")
    private FacultyInfo info;

}
