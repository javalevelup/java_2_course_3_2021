package org.levelup.univeristy.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// @SuppressWarnings("ALL")
public class SteamApiExamples {

    public static void main(String[] args) {
        // SteamAPI

        Collection<String> products = new ArrayList<>();
        products.add("Молоко");
        products.add("Хлеб");
        products.add("Сметана");
        products.add("Помидоры");
        products.add("Сыр");
        products.add("Колбаса");
        products.add("Йогурт");

        // 1. Сортировки
        List<String> ascSortedProducts = products.stream()
                .sorted()
                .collect(Collectors.toList());
        printCollection(ascSortedProducts);

        List<String> descSortedProducts = products.stream()
                .sorted((p1, p2) -> -p1.compareTo(p2))
                .collect(Collectors.toList());
        printCollection(descSortedProducts);

        List<String> ascStringLenghtProducts = products.stream()
                // .sorted((p1, p2) -> Integer.compare(p1.length(), p2.length()))
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());
        printCollection(ascStringLenghtProducts);

        // 2. Преобразования + фильтрация
        List<Integer> lengths = products.stream()
                .map(String::length) // Stream<Integer> // incoming_argument.anyMethod()
                .filter(length -> length > 4)
                .collect(Collectors.toList());
        printCollection(lengths);

        List<String> productsStartWithC = products.stream()
                // .map(product -> product.toUpperCase()) // Stream<String>
                .map(String::toUpperCase) // Stream<String>
                .filter(product -> product.startsWith("С")) // C должна быть русской!
                .collect(Collectors.toList());
        printCollection(productsStartWithC);


//        Stream<String> stream = products.stream()
//                .sorted();
//        /// ....
//        /// ....
//        /// ....
//        stream.collect(Collectors.toList());
    }

    private static void printCollection(Collection<?> collection) {
        System.out.println();
        // collection.forEach(val -> System.out.println(val));
        collection.forEach(System.out::println);
    }

}
