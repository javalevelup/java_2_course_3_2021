package org.levelup.univeristy.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LambdaFunctions {

    @SuppressWarnings("Convert2MethodRef")
    public static void main(String[] args) {

        List<Integer> values = new ArrayList<>();

        values.add(2);
        values.add(1);
        values.add(8);
        values.add(2);
        values.add(13);
        values.add(5);
        values.add(3);
        values.add(21);

//        values.sort(new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                // <0 - o1 < o2: -1
//                // 0  - o1 = o2: 0
//                // >0 - o1 > o2: 1
//                return -Integer.compare(o1, o2);
//            }
//        });

        values.sort((o1, o2) -> -Integer.compare(o1, o2));
        values.sort((o1, o2) -> {
            System.out.println(o1 + " " + o2);
            return -Integer.compare(o1, o2);
        });

        System.out.println(values);

        int minBound = 10;
//        values.removeIf(new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                return integer < minBound;
//            }
//        });

        // 1. Интерфейс должен иметь один метод, который нужно переопределить
        // 2. (необязательное условие) Над интерфейсом стоит аннотация @FunctionalInterface

        // (method arguments...) -> { method body }
        // (method arguments...) -> expression

//      values.removeIf(new Predicate<Integer>() {
//          @Override
//          public boolean test(Integer val) {
//              return val < minBound;
//          }
//      });

//      values.removeIf((val) -> {
//          return val < minBound;
//      });
        values.removeIf(val -> val < minBound);
        System.out.println(values);


        System.out.println();
        System.out.println("foreach loop");
        for (Integer val : values) {
            System.out.println(val);
        }

        System.out.println();
        System.out.println("lambda foreach");
        values.forEach(val -> System.out.println("val: " + val));

        List<String> resultList = new ArrayList<>();
        for (Integer val : values) {
            String result = Integer.toHexString(val) + " " + Integer.toOctalString(val);
            resultList.add(result);
        }

        List<String> hexOctValues = values.stream()
                .map(val -> Integer.toHexString(val) + " " + Integer.toOctalString(val))
                .collect(Collectors.toList());

        System.out.println("Hex/Oct values");
        hexOctValues.forEach(val -> System.out.println("hex/oct: " + val));


//        List<Integer> v = new ArrayList<>() {
//            @Override
//            public boolean add(Integer integer) {
//                System.out.println("Value: " + integer);
//                return super.add(integer);
//            }
//        };
//        v.add(5);
//        v.add(6);

    }

}
