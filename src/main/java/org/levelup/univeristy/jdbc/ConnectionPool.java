package org.levelup.univeristy.jdbc;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.Queue;

public class ConnectionPool {

    private Queue<Connection> queue = new LinkedList<>();

    // Вызываем, когда нам необходимо соединение
    public Connection getConnection() {
        if (queue.isEmpty()) {
            return null;
        }
        return queue.poll(); // достаем connection из очереди
    }

    // Вызываем, когда вернуть соединение в pool
    public void returnConnection(Connection connection) {
        queue.offer(connection); // кладем connection в очередь
    }

}
