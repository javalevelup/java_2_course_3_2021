package org.levelup.univeristy.jdbc;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

@RequiredArgsConstructor
public class DatabaseServiceInvocationHandler implements InvocationHandler {

    private final DefaultDatabaseService service;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(service, args); // result - instance of java.sql.Connection
        long startTime = System.nanoTime(); // time of getting connection
        ConnectionTimeStorage.getInstance().put((Connection) result, startTime);
        return result;
    }

}
