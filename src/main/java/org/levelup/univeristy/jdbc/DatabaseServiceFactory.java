package org.levelup.univeristy.jdbc;

import org.levelup.univeristy.configuration.DatabaseConfiguration;

import java.lang.reflect.Proxy;

public class DatabaseServiceFactory {

    public static DatabaseService getDatabaseService() {
        return (DatabaseService) Proxy.newProxyInstance(
                DefaultDatabaseService.class.getClassLoader(),
                DefaultDatabaseService.class.getInterfaces(),
                new DatabaseServiceInvocationHandler(new DefaultDatabaseService(DatabaseConfiguration.getInstance()))
        );
    }

}
