package org.levelup.univeristy.jdbc;

import java.sql.Connection;
import java.util.IdentityHashMap;
import java.util.Map;

public final class ConnectionTimeStorage {

    private ConnectionTimeStorage() {}

    private static final ConnectionTimeStorage INSTANCE = new ConnectionTimeStorage();

    public static ConnectionTimeStorage getInstance() {
        return INSTANCE;
    }

    private Map<Connection, Long> connectionOpenTimes = new IdentityHashMap<>(); // use == instead of equals

    public void put(Connection connection, long startTime) {
        connectionOpenTimes.put(connection, startTime);
    }

    public long get(Connection connection) {
        Long startTime = connectionOpenTimes.get(connection); // (Long)null -> autoboxing -> NPE
        return startTime == null ? 0L : startTime;
    }

}
