package org.levelup.univeristy.jdbc;

import java.sql.Connection;

public interface DatabaseService {

    Connection openConnection();

}
