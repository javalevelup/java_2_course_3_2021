package org.levelup.univeristy.jdbc;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

// CGLIB
// ByteBuddy
// Javasist
public class ConnectionInvocationHandler implements InvocationHandler {

    private final Connection originalConnection;
    private final ConnectionPool connectionPool;

    public ConnectionInvocationHandler(Connection originalConnection, ConnectionPool connectionPool) {
        this.originalConnection = originalConnection;
        this.connectionPool = connectionPool;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        // void close();
        if (methodName.equals("close")) {
            connectionPool.returnConnection(originalConnection);
            long startTime = ConnectionTimeStorage.getInstance().get(originalConnection);
            System.out.println(System.nanoTime() - startTime);
            return null;
        }

        // Вызов метода у оригинального объекта
        return method.invoke(originalConnection, args);
    }

}
