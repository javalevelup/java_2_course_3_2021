package org.levelup.univeristy.configuration;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.levelup.univeristy.domain.Faculty;
import org.levelup.univeristy.domain.University;
import org.postgresql.Driver;

import java.util.HashMap;
import java.util.Map;

public class HibernateConfiguration {

    private static SessionFactory factory;

    // Настройка Hibernate
    public static void configure(DatabaseConfiguration dbConfiguration) {
        Map<String, String> hibernateProperties = buildHibernateProperties(dbConfiguration);
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder()
                .applySettings(hibernateProperties)
                .build();

        Configuration cfg = new Configuration()
                .addAnnotatedClass(University.class)
                .addAnnotatedClass(Faculty.class);

        factory = cfg.buildSessionFactory(ssr);
    }

    private static Map<String, String> buildHibernateProperties(DatabaseConfiguration dbConfiguration) {
        Map<String, String> properties = new HashMap<>();

        properties.put("hibernate.connection.driver_class", Driver.class.getName());

        properties.put("hibernate.connection.url", dbConfiguration.getUrl());
        properties.put("hibernate.connection.username", dbConfiguration.getLogin());
        properties.put("hibernate.connection.password", dbConfiguration.getPassword());

        properties.put("hibernate.show_sql", "true");         // Выводит в консоль все запросы, которые выполняет Hibernate
        properties.put("hibernate.format_sql", "true");       // Выводит запросы в более читаемом виде

        // validate
        // update
        // create
        // create-drop
        properties.put("hibernate.hbm2ddl.auto", "validate"); // Отвечает за генерацию схемы

        return properties;
    }

    public static SessionFactory getSessionFactory() {
        if (factory == null) {
            throw new RuntimeException("SessionFactory isn't configured");
        }
        return factory;
    }

}
