package org.levelup.univeristy;

import org.hibernate.SessionFactory;
import org.levelup.univeristy.configuration.DatabaseConfiguration;
import org.levelup.univeristy.configuration.HibernateConfiguration;
import org.levelup.univeristy.domain.University;
import org.levelup.univeristy.reflect.AnnotationConfigurationPropertiesProcessor;
import org.levelup.univeristy.repository.UniversityRepository;
import org.levelup.univeristy.repository.hbm.HibernateUniversityRepository;

import java.util.List;

public class HibernateExamples {

    public static void main(String[] args) {
        String configurationFilename = "database.properties";
        AnnotationConfigurationPropertiesProcessor.processConfigurationFile(configurationFilename);
        System.out.println("Application loaded all configuration files");
        HibernateConfiguration.configure(DatabaseConfiguration.getInstance());
        System.out.println("Hibernate has been configured successfully");
        System.out.println(DatabaseConfiguration.getInstance().toString());
        SessionFactory factory = HibernateConfiguration.getSessionFactory();
        System.out.println("University application has been started");


        UniversityRepository universityRepository = new HibernateUniversityRepository(factory);
        University u = universityRepository.createUniversity(
                "Российский новый университет",
                "РНУ",
                1991,
                List.of("Гуманитарный интситут", "Налоговый институт")
        );
        System.out.println(u);


//        FacultyRepository facultyRepository = new HibernateFacultyRepository(factory);
//        Faculty faculty = facultyRepository.createFaculty("ФТК", 2L);
//        System.out.println(faculty);

        // get/load
//        University getU = null;
//        University loadU = null;
//
//        try (Session s = factory.openSession()) {
//            getU = s.get(University.class, 2030L);
//        }
//
//        try (Session s = factory.openSession()) {
//            loadU = s.load(University.class, 2030L);
//            System.out.println();
//        }
//
//        System.out.println(getU);
//        System.out.println(loadU);

        factory.close();
    }

}
