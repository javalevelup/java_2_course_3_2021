package org.levelup.univeristy.threads.interruption;

public class Worker implements Runnable {

    @Override
    public void run() {
        int interruptCallCount = 0;
        boolean isInterrupted = false;

        while (true) {
            try {
                // void interrupt()
                // boolean isInterrupted()
                // static boolean interrupted()
                if (isInterrupted) {
                    interruptCallCount++;
                    System.err.println("Count of interruption calls: " + interruptCallCount);
                    if (interruptCallCount >= 3) {
                        System.err.println("Thread finishes its work");
                        return;
                    }
                }

                isInterrupted = Thread.interrupted();
                System.err.println("Thread is working...");
                Thread.sleep(700);
            } catch (InterruptedException exc) {
                System.err.println("Thread has been interrupted...");
                isInterrupted = true;
            }
        }
    }

}
