package org.levelup.univeristy.threads;

public class MoneyTransfer {

    public void transferMoney(Integer acc1, Integer acc2, double amount) {
        Integer firstLocked = acc1 > acc2 ? acc2 : acc1;
        Integer secondLocked = acc1 > acc2 ? acc1 : acc2;

        synchronized (firstLocked) {
            synchronized (secondLocked) {
                // ... transfer money
                System.out.println(amount);
            }
        }
    }

    public static void main(String[] args) {
        MoneyTransfer mt = new MoneyTransfer();

        Integer firstAccount = 100;
        Integer secondAccount = 101;

        mt.transferMoney(firstAccount, secondAccount, 54.23);
        // t1: acquire firstAccount, wait for secondAccount -> t1: acquire firstAccount, acquire secondAccount
        mt.transferMoney(secondAccount, firstAccount, 435.23);
        // t2: acquire secondAccount, wait for firstAccount -> t2: try acquire firstAccount -> wait
    }

}
