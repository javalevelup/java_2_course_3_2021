package org.levelup.univeristy.threads.pc;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StatisticRequestConsumer implements Runnable {

    private final StatisticRequestQueue queue;

    @Override
    public void run() {
        boolean isInterrupted = false;

        // interrupted = false -> execute iteration
        // interrupted = true -> check queue -> !queue.isEmpty - execute iteration
        // interrupted = true -> check queue -> queue.isEmpty - finish work
        while (!isInterrupted || !queue.isEmpty()) {
            try {
                String accountNumber = queue.take();
                System.err.println("Consumer " + Thread.currentThread().getName() +
                        " start collect statistics for the account " + accountNumber);
                Thread.sleep(1500); // 1.5s
                System.err.println("Consumer " + Thread.currentThread().getName() +
                        " collected statistics for the account " + accountNumber);

                if (!isInterrupted) {
                    isInterrupted = Thread.interrupted();
                }
            } catch (InterruptedException exc) {
                System.out.println("Thread has been interrupted...");
                isInterrupted = true;
            }
        }


    }

}
