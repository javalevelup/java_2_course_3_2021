package org.levelup.univeristy.threads.pc;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockStatisticRequestQueue implements StatisticRequestQueue {

    private final LinkedList<String> queue = new LinkedList<>();

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition emptyCondition = lock.newCondition();
    private final Condition fullCondition = lock.newCondition();

    // private final Object fullConditionMutex = new Object();
    // private final Object emptyConditionMutex = new Object();

    private final int queueSize;

    public ReentrantLockStatisticRequestQueue(int queueSize) {
        this.queueSize = queueSize;
    }

    // queue is empty
    // con1, con2 are waiting (take())
    // pro1 offer
    // send signal to wake up consumers
    @Override
    public void offer(String accountNumber) throws InterruptedException {
        lock.lock();
        try {
            while (queue.size() >= queueSize) {
                fullCondition.await();
                // synchronized (fullConditionMutex) { fullConditionMutex.wait(); }
            }

            queue.addLast(accountNumber);
            emptyCondition.signalAll();
            // synchronized (emptyConditionMutex) { emptyConditionMutex.notifyAll(); }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String take() throws InterruptedException {
        lock.lock();
        try {
            while (queue.isEmpty()) {
                emptyCondition.await();
                // synchronized (emptyConditionMutex) { emptyConditionMutex.wait(); }
            }

            String accountNumber = queue.poll();
            fullCondition.signalAll();
            // synchronized (fullConditionMutex) { fullConditionMutex.notifyAll(); }
            return accountNumber;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        lock.lock();
        try {
            return queue.isEmpty();
        } finally {
            lock.unlock();
        }
    }

}
