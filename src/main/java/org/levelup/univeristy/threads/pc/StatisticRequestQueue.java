package org.levelup.univeristy.threads.pc;

public interface StatisticRequestQueue {

    // add to queue
    void offer(String accountNumber) throws InterruptedException;

    // get the next accountNumber from queue
    String take() throws InterruptedException;

    boolean isEmpty();

}
