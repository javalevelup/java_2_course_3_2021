package org.levelup.univeristy.threads;

import java.util.concurrent.atomic.AtomicInteger;

// CAS - compare and set/swap
// original: 100

// p1: t1: r1 = read 100
// t1: val = r1 + 1 = 101
// t1: cas(r1, val)
//      r2 = read
//      if (r1 == r2) set(val)
//      if (r1 != r2) goto p1
public class NonBlockingCounter implements Counter {

    private final AtomicInteger value = new AtomicInteger(0);

    @Override
    public void increment() {
        value.incrementAndGet();
    }

    @Override
    public int getValue() {
        return value.get();
    }

}
