package org.levelup.univeristy.threads;

public interface Counter {

    void increment();

    int getValue();

}
