package org.levelup.univeristy.threads;

import lombok.SneakyThrows;

import java.time.LocalTime;

public class ThreadsExample {

    @SneakyThrows
    public static void main(String[] args) {
        Thread timePrinter = new Thread(new TimePrinter());
        timePrinter.setDaemon(true); // now it's a daemon-thread
        timePrinter.start();

        // main thread
        String mainThreadName = Thread.currentThread().getName();
        System.out.println(mainThreadName + ": start method main");

        Thread thread = new Printer();
        // thread.run();
        thread.start(); // create new thread, run thread

        Thread factorialThread = new Thread(new FactorialCalculator(), "factorial-thread"); // 2nd arg: thread name
        factorialThread.start();

        Thread ft2 = new Thread(new FactorialCalculator(), "factorial-thread-2");
        Thread ft3 = new Thread(new FactorialCalculator(), "factorial-thread-3");
        ft2.start();
        ft3.start();

        thread.join(); // поток main будет ожидать, пока thread не завершится

        Thread.sleep(2000); // 1500ms - 1,5s
        System.out.println(mainThreadName + ": at the end of method main");
    }

    static class Printer extends Thread {

        public Printer() {
            super("printer-thread");
        }

        @Override
        @SneakyThrows
        public void run() {
            String threadName = getName();
            for (int i = 0; i < 10; i++) {
                System.out.println(threadName + ": In custom thread");
                Thread.sleep(400); // 400ms
            }
            System.out.println(threadName + ": Method run finished invocation");
        }

    }

    static class FactorialCalculator implements Runnable {

        @Override
        @SneakyThrows
        public void run() {
            int factorial = 1;
            for (int i = 1; i < 9; i++) {
                factorial *= i;
            }

            Thread.sleep(600); // 600ms
            System.out.println(Thread.currentThread().getName() + ": factorial = " + factorial);
        }

    }

    static class TimePrinter implements Runnable {

        @Override
        @SneakyThrows
        public void run() {
            while (true) {
                System.out.println(LocalTime.now());
                Thread.sleep(1000); // 1000ms - 1s
            }
        }

    }

}
