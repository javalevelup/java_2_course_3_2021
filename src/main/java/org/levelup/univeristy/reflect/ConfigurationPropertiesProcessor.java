package org.levelup.univeristy.reflect;

import org.levelup.univeristy.configuration.DatabaseConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationPropertiesProcessor {

    public static void processConfigurationFile(String filename) {
        // /Users/dmitryprotsko/IdeaProjects/level-up/java_2_course_3_2021/src/main/resources/database.properties
        // /Users/dmitryprotsko/IdeaProjects/level-up/java_2_course_3_2021/target/java_2_course_3_2021.jar!/database.properties
        InputStream is = ConfigurationPropertiesProcessor.class.getClassLoader().getResourceAsStream(filename);
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is))) {
            Map<String, Object> configurationProperties = readProperties(fileReader);
            fillConfiguration(configurationProperties);
        } catch (IOException exc) {
            System.out.println("An error occurred during reading " + filename + " file.");
            throw new RuntimeException(exc);
        } catch (IllegalAccessException exc) {
            System.out.println("Couldn't set property value to object field");
            throw new RuntimeException(exc);
        }
    }

    // String (Key) - property key
    // Object (Value) - property value
    private static Map<String, Object> readProperties(BufferedReader reader) throws IOException {
        Map<String, Object> properties = new HashMap<>();

        String line;
        while ( (line = reader.readLine()) != null ) {
            if (!line.isBlank()) { // isBlank() -> trim().isEmpty() -> "   " -> true

                String[] elements = line.split("="); // "database.password = root".split("=") -> String["database.password ", " root"]
                properties.put(
                        // database.connection.timeout -> connectiontimeout
                        elements[0].trim().replace("database", "").replace(".", ""),
                        elements[1].trim()
                );
            }
        }

        return properties;
    }

    private static void fillConfiguration(Map<String, Object> properties) throws IllegalAccessException {
        Class<?> dbConfigurationClass = DatabaseConfiguration.class;

        Field[] fields = dbConfigurationClass.getDeclaredFields();
        for (Field field : fields) {
            if (!field.getName().equalsIgnoreCase("instance")) {
                String lowerCaseFieldName = field.getName().toLowerCase(); // "connectionTimeout".toLowerCase() -> "connectiontimeout"
                Object propertyValue = properties.get(lowerCaseFieldName);

                field.setAccessible(true); // allow set value to private field
                field.set(DatabaseConfiguration.getInstance(), castStringToFieldType(field.getType(), propertyValue));
            }
        }

    }

    private static Object castStringToFieldType(Class<?> fieldType, Object propertyValue) {
        if (fieldType == String.class) {
            return propertyValue;
        }

        if (fieldType.isPrimitive() && fieldType != boolean.class) {
            return Integer.parseInt((String) propertyValue);
        }
        return null;
    }

}
