package org.levelup.univeristy.reflect;

import org.levelup.univeristy.configuration.DatabaseConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("DuplicatedCode")
public class AnnotationConfigurationPropertiesProcessor {

    public static void processConfigurationFile( String filename) {
        // /Users/dmitryprotsko/IdeaProjects/level-up/java_2_course_3_2021/src/main/resources/database.properties
        // /Users/dmitryprotsko/IdeaProjects/level-up/java_2_course_3_2021/target/java_2_course_3_2021.jar!/database.properties
        InputStream is = AnnotationConfigurationPropertiesProcessor.class.getClassLoader().getResourceAsStream(filename);
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is))) {
            Map<String, String> configurationProperties = readProperties(fileReader);
            fillConfiguration(configurationProperties);
        } catch (IOException exc) {
            System.out.println("An error occurred during reading " + filename + " file.");
            throw new RuntimeException(exc);
        } catch (IllegalAccessException exc) {
            System.out.println("Couldn't set property value to object field");
            throw new RuntimeException(exc);
        }
    }

    private static Map<String, String> readProperties(BufferedReader reader) throws IOException {
        Map<String, String> properties = new HashMap<>();

        String line;
        while ( (line = reader.readLine()) != null ) {
            if (!line.isBlank()) { // isBlank() -> trim().isEmpty() -> "   " -> true

                String[] elements = line.split("="); // "database.password = root".split("=") -> String["database.password ", " root"]
                properties.put(
                        elements[0].trim(),
                        elements[1].trim()
                );
            }
        }

        return properties;
    }

    private static void fillConfiguration(Map<String, String> properties) throws IllegalAccessException {
        Class<?> dbConfigurationClass = DatabaseConfiguration.class;

        Field[] fields = dbConfigurationClass.getDeclaredFields();
        for (Field field : fields) {
            Property annotation = field.getAnnotation(Property.class);
            if (annotation != null) {
                String key = annotation.key(); // получаем значение аннотации у поля field
                String propertyValue = properties.get(key);

                field.setAccessible(true);
                field.set(DatabaseConfiguration.getInstance(), castStringToFieldType(field.getType(), propertyValue));
            }
        }

    }

    private static Object castStringToFieldType(Class<?> fieldType, String propertyValue) {
        if (fieldType == String.class) {
            return propertyValue;
        }

        if (fieldType.isPrimitive() && fieldType != boolean.class) {
            return propertyValue == null ? 0 : Integer.parseInt(propertyValue);
        }
        return null;
    }

}
