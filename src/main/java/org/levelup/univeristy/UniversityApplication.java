package org.levelup.univeristy;

import org.hibernate.SessionFactory;
import org.levelup.univeristy.configuration.DatabaseConfiguration;
import org.levelup.univeristy.configuration.HibernateConfiguration;
import org.levelup.univeristy.jdbc.DatabaseService;
import org.levelup.univeristy.jdbc.DatabaseServiceFactory;
import org.levelup.univeristy.reflect.AnnotationConfigurationPropertiesProcessor;

public class UniversityApplication {

    public static void main(String[] args) {
        String configurationFilename = "database.properties";
        AnnotationConfigurationPropertiesProcessor.processConfigurationFile(configurationFilename);
        System.out.println("Application loaded all configuration files");
        HibernateConfiguration.configure(DatabaseConfiguration.getInstance());
        System.out.println("Hibernate has been configured successfully");
        System.out.println(DatabaseConfiguration.getInstance().toString());
        SessionFactory factory = HibernateConfiguration.getSessionFactory();
        System.out.println("University application has been started");






//        UniversityRepository universityRepository = new HibernateUniversityRepository(factory);
//        List<University> universities = universityRepository.findAll();
//        for (University u : universities) {
//            System.out.println(u);
//        }
//        University u = universityRepository.createUniversity("Высшая Школа Экономики", "ВШЭ", null);
//        System.out.println(u);

        factory.close();

        DatabaseService dbService = DatabaseServiceFactory.getDatabaseService();
//        dbService.fillPool();
//        System.out.println("Connection pool has been initialized");
//        DatabaseService dbService = new DatabaseService();
//        UniversityRepository universityRepository = new JdbcUniversityRepository(dbService);
//
//        University itmo = universityRepository.createUniversity("Универстиет ИТМО", "ИТМО", 1900);
//        System.out.println("Newly inserted university: " + itmo.getUniversityId());
//
//        List<University> universities = universityRepository.findAll();
//        for (University u : universities) {
//            System.out.println(u.getShortName() + " " + u.getFoundationYear());
//        }
//
//        Connection connection = dbService.openConnection();
//        System.out.println("Connection has been opened");
//        connection.close();
//        System.out.println("Connection has been closed");
    }

}
