package org.levelup.univeristy.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;

public class JavaReflectionExamples {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        // ClassLoader.loadClass(Subject) -> Class<Subject>
        Subject subject = new Subject("Программирование");
        Object obj = subject;
        Class<?> objClass = obj.getClass(); // -> Class<Subject>

        // 1. Через объект
        Class<?> subjClass = subject.getClass();  // -> Class<Subject>
        // 2. Через имя класса и литерал .class
        Class<?> subjectClass = Subject.class;


        Field[] fields = subjectClass.getDeclaredFields();
        for (Field field : fields) {
            // String name; -> field.getType() -> Class<String> sClass -> sClass.getName()
            System.out.println(field.getType().getName() + " " + field.getName());
        }

        System.out.println("Constructors...");
        Constructor<?>[] constructors = subjectClass.getDeclaredConstructors();

        for (Constructor<?> constructor : constructors) {
            // Subject() -> []
            // Subject(String name) -> [Class<String>]
            // Subject(String name, int hours) -> [Class<String>, Class<int>]
            Class<?>[] types = constructor.getParameterTypes();
            System.out.println(Arrays.toString(types));
        }

        // subject(...)
        Field subjectNameField = subjectClass.getDeclaredField("name");
        subjectNameField.setAccessible(true);
        String subjectName = (String) subjectNameField.get(subject);
        System.out.println(subjectName);
        subjectNameField.set(subject, "Programming");
        System.out.println((String) subjectNameField.get(subject));
    }

}
